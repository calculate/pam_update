INSTALL	:= install
DESTDIR	:=
PIC	:= -fPIC
LD_D	:= $(CC) -shared
CFLAGS	:=
SHARE	:= -g $(PIC)
MODULENAME	:= update
PAMMODULE	:= pam_$(MODULENAME)


$(PAMMODULE): $(PAMMODULE).c
	$(CC) $(CFLAGS) $(SHARE) -c $(PAMMODULE).c -o $(PAMMODULE).o
	$(LD_D)  -o $(PAMMODULE).so $(PAMMODULE).o  -lc -lpam

clean: $(PAMMODULE).so $(PAMMODULE).o
	$(RM) $(PAMMODULE).o
	$(RM) $(PAMMODULE).so
	
install: $(PAMMODULE).so
	$(INSTALL) -D -m 0755 $(PAMMODULE).so $(DESTDIR)/lib/security/$(PAMMODULE).so

uninstall: $(DESTDIR)/lib/security/$(PAMMODULE).so
	$(RM) $(DESTDIR)/lib/security/$(PAMMODULE).so
